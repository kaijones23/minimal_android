#include <glob.h>
#include <vector>
#include <string>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <sstream>

#ifdef EXTERNAL_SYMBOLS
// Convert EXTERNAL_SYMBOLS macro to a string if defined
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
const std::string externalSymbolsStr = TOSTRING(EXTERNAL_SYMBOLS);
#else
const std::string externalSymbolsStr = "";
#endif

int main() {
    // Initialize vector to hold default library paths
    std::vector<std::string> libraryPaths = {
#ifdef RECOVERY_LINKER
        "/usr/lib",
        "/lib64"
#else
        "/lib",
        "/lib64",
        "/usr/lib",
        "/usr/lib64",
        "/usr/local/lib",
        "/usr/local/lib64"
#endif
    };

#ifdef LEGACY_USERSPACE
    libraryPaths.push_back("/lib/legacy");
    libraryPaths.push_back("/lib64/legacy");
    // Include the legacy ld configuration for Linux
    libraryPaths.push_back("/etc/ld.so.conf.d/legacy_ld.conf");
#endif

#ifdef LD_SHIM_LIBS
    libraryPaths.push_back(LD_SHIM_LIBS);
#endif

    // Support for external locations using environment variable "EXTERNAL_LIB_PATHS"
    const char* externalLibPathsEnv = std::getenv("EXTERNAL_LIB_PATHS");
    std::vector<std::string> externalLibPaths;

    if (externalLibPathsEnv) {
        std::string paths(externalLibPathsEnv);
        size_t pos = 0;
        while ((pos = paths.find(':')) != std::string::npos) {
            externalLibPaths.push_back(paths.substr(0, pos));
            paths.erase(0, pos + 1);
        }
        externalLibPaths.push_back(paths); // Add last remaining path
    }

    // Combine library paths with external paths
    libraryPaths.insert(libraryPaths.end(), externalLibPaths.begin(), externalLibPaths.end());

    // Output the library paths list
    printf("A helper program for linking dynamic executables.\n");
    printf("Library paths loaded into the list:\n");
    for (const auto& path : libraryPaths) {
        printf("%s\n", path.c_str());
    }

    // Parse and print external symbols
    printf("\nExternal symbols loaded:\n");
    if (!externalSymbolsStr.empty()) {
        std::istringstream symbolsStream(externalSymbolsStr);
        std::string symbol;
        while (symbolsStream >> symbol) {
            printf("%s\n", symbol.c_str());
        }
    } else {
        printf("No external symbols defined.\n");
    }

    return 0;
}
