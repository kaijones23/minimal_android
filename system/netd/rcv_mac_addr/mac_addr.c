// get_mac.c
# define LOG_TAG "modem_manager"
#include <net/if.h>  //ifreq
#include <stdio.h>   //printf
#include <string.h>  //strncpy
#include <sys/ioctl.h>
#include <log.h>
# include "/run/media/kjones/DE5839AB58398377/minimal_android/system/core/liblog/logger.c"
#include <sys/socket.h>
#include <unistd.h>  //close
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <linux/wireless.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>


int check_enp3s0() {
  int fd;
  struct ifreq ifr;
  char *iface = "enp3s0";
  unsigned char *mac = NULL;

  memset(&ifr, 0, sizeof(ifr));

  fd = socket(AF_INET, SOCK_DGRAM, 0);

  ifr.ifr_addr.sa_family = AF_INET;
  strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);

  if (0 == ioctl(fd, SIOCGIFHWADDR, &ifr)) {
    mac = (unsigned char *)ifr.ifr_hwaddr.sa_data;
    LOGD("checking enp3s0 interface");
    LOGI("local mac address : %.2X:%.2X:%.2X:%.2X:%.2X:%.2X", mac[0], mac[1],
           mac[2], mac[3], mac[4], mac[5]);
  }

  close(fd);

  return 0;
}

int check_wlp4s0() {
  int fd;
  struct ifreq ifr;
  char *iface = "wlp4s0";
  unsigned char *mac = NULL;

  memset(&ifr, 0, sizeof(ifr));

  fd = socket(AF_INET, SOCK_DGRAM, 0);

  ifr.ifr_addr.sa_family = AF_INET;
  strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);

  if (0 == ioctl(fd, SIOCGIFHWADDR, &ifr)) {
    mac = (unsigned char *)ifr.ifr_hwaddr.sa_data;
    LOGD("checking wlp4s0 interface");
    LOGI("wifi mac address : %.2X:%.2X:%.2X:%.2X:%.2X:%.2X", mac[0], mac[1],
           mac[2], mac[3], mac[4], mac[5]);
  }

  close(fd);

  return 0;
}

int check_lo() {
  int fd;
  struct ifreq ifr;
  char *iface = "lo";
  unsigned char *mac = NULL;

  memset(&ifr, 0, sizeof(ifr));

  fd = socket(AF_INET, SOCK_DGRAM, 0);

  ifr.ifr_addr.sa_family = AF_INET;
  strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);

  if (0 == ioctl(fd, SIOCGIFHWADDR, &ifr)) {
    mac = (unsigned char *)ifr.ifr_hwaddr.sa_data;
    LOGD("checking loopback interface");
    LOGI("loopback mac address : %.2X:%.2X:%.2X:%.2X:%.2X:%.2X", mac[0], mac[1],
           mac[2], mac[3], mac[4], mac[5]);
  }

  close(fd);

  return 0;
}




int check_wireless(const char* ifname, char* protocol) {
  int sock = -1;
  struct iwreq pwrq;
  memset(&pwrq, 0, sizeof(pwrq));
  strncpy(pwrq.ifr_name, ifname, IFNAMSIZ);

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket");
    return 0;
  }

  if (ioctl(sock, SIOCGIWNAME, &pwrq) != -1) {
    if (protocol) strncpy(protocol, pwrq.u.name, IFNAMSIZ);
    close(sock);
    return 1;
  }

  close(sock);
  return 0;
}


int check_adapters() {
  struct ifaddrs *ifaddr, *ifa;

  if (getifaddrs(&ifaddr) == -1) {
    LOGE("getifaddrs");
    return -1;
  }

  /* Walk through linked list, maintaining head pointer so we
     can free list later */
  for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
    char protocol[IFNAMSIZ]  = {0};

    if (ifa->ifa_addr == NULL ||
        ifa->ifa_addr->sa_family != AF_PACKET) continue;

    if (check_wireless(ifa->ifa_name, protocol)) {
      LOGD("interface %s is wireless: %s", ifa->ifa_name, protocol);
    } else {
      LOGE("interface %s is not wireless", ifa->ifa_name);
    }
  }

  freeifaddrs(ifaddr);
  return 0;
}

int loopback(){
check_adapters();
check_enp3s0();
check_wlp4s0();
check_lo();
}

int main() {
loopback();
wait(NULL);
}


