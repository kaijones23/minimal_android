#include <stdio.h>
# include <stdio.h>
# include <stdlib.h> 
# include <dirent.h>
# include "log.h"
# include "vold.h"
# define LOG_TAG "212 245  vold        "
# define VOLD_NAME " The Dead"

int load_loop1 () {
    printf(ALOGD" vold "VOLD_NAME" is winding up...\n");
    struct dirent *de;
    DIR *dr = opendir("dev/block/loop0"); 
  
    while (dr == NULL)
    { 
        printf(ALOGE "Unable to get LOOP_STATUS_64 for %s  \n" ); 
        return 0; 
    }
    while ((de = readdir(dr)) != NULL) 
            printf(ALOGI "Found Mount namespace for %s\n", de->d_name); 
    closedir(dr);     
    return 0; 
}
int load_loop2 () {
    struct dirent *de;
    DIR *dr = opendir("dev/block/loop1"); 
  
    while (dr == NULL)
    { 
        printf(ALOGE "Unable to get LOOP_STATUS_64 for %s  \n" ); 
        return 0; 
    } 
    while ((de = readdir(dr)) != NULL) 
            printf(ALOGI "Found Mount namespace for %s\n", de->d_name); 
    closedir(dr);     
    return 0; 
}

int load_loop3 () {
    struct dirent *de;
    DIR *dr = opendir("dev/block/loop2"); 
  
    while (dr == NULL)
    { 
        printf(ALOGE "Unable to get LOOP_STATUS_64 for %s  \n" ); 
        return 0; 
    } 
    while ((de = readdir(dr)) != NULL) 
            printf(ALOGI "Found Mount namespace for %s\n", de->d_name); 
    closedir(dr);     
    return 0; 
}

int load_loop4 () {
    struct dirent *de;
    DIR *dr = opendir("dev/block/loop3"); 
  
    while (dr == NULL)
    { 
        printf(ALOGE "Unable to get LOOP_STATUS_64 for %s  \n" ); 
        return 0; 
    } 
    while ((de = readdir(dr)) != NULL) 
            printf(ALOGI "Found Mount namespace for %s\n", de->d_name); 
    closedir(dr);     
    return 0; 
}


int load_loop5() {
    struct dirent *de;
    DIR *dr = opendir("dev/block/loop4"); 
  
    while (dr == NULL) 
    { 
        printf(ALOGE "Unable to get LOOP_STATUS_64 for %s  \n" ); 
        return 0; 
    }  
    while ((de = readdir(dr)) != NULL) 
            printf(ALOGI "Found Mount namespace for %s\n", de->d_name); 
    closedir(dr);     
    return 0; 
}

int load_loop () {
load_loop1();
load_loop2();
load_loop3();
load_loop4();
load_loop5();
}
