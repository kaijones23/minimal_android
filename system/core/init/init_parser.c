#define LOG_TAG "init_parser"
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include  <string.h>
#include <sys/stat.h>
#include "log.h"
int system_init()
{
	struct dirent *de;
	char * file_name;
	DIR *dr = opendir("system/etc/init/");

	while (dr == NULL) {
		LOGE(
		       "Unable to read config file '/system/etc/init': open() failed: No such file or directory");
		return 0;
	}
	while ((de = readdir(dr)) != NULL) {
	 if ( !strcmp(de->d_name, ".") || !strcmp(de->d_name, "..") )
        {
            // do nothing (straight logic)
        } else {
            file_name = de->d_name; // use it
		/*parse init.rc then do another init tasks*/
		LOGD( "Parsing file %s ...", de->d_name);
//		system("%s", de->d_name)
}
}
	closedir(dr);
	system("/bin/bash -x system/etc/init/hw/init.rc");
	return 0;
}

int vendor_init_rc()
{
	struct dirent *de;
	DIR *dr = opendir("vendor/etc/init/");

	while (dr == NULL) {
		LOGE(
		       "Unable to read config file '/vendor/etc/init': open() failed: No such file or directory");
		return 0;
	}
	while ((de = readdir(dr)) != NULL)
 if ( !strcmp(de->d_name, ".") || !strcmp(de->d_name, "..") )
        {
            // do nothing (straight logic)
        } else {

		LOGD( "Parsing file %s ...", de->d_name);
	closedir(dr);
	return 0;
}
}

/*
 * Check if a file exist using fopen() function
 * return 1 if the file exist otherwise return 0
 */
int cfileexists(const char * filename){
    /* try to open file to read */
    FILE *file;
    if (file = fopen(filename, "r")){
        fclose(file);
        return 1;
    }
    return 0;
}


int recovery_init()
{
 char* filename = "init.rc";
    int exist = cfileexists(filename);
    if(exist) {
        LOGD("Parsing file %s ...",filename);
        system("/bin/bash init.rc");
}
    if (!exist) {
        LOGE("Unable to read config file '/%s': open() failed: No such file or directory",filename);
}
}

int product_init()
{
	struct dirent *de;
	DIR *dr = opendir("product/etc/init/");

	while (dr == NULL) {
		LOGE(
		       "Unable to read config file '/product/etc/init': open() failed: No such file or directory");
		return 0;
	}
	while ((de = readdir(dr)) != NULL)

		LOGD( "Parsing file %s ...", de->d_name);
	closedir(dr);
	return 0;
}

int odm_init()
{
	struct dirent *de;
	DIR *dr = opendir("odm/etc/init/");

	while (dr == NULL) {
		LOGE(
		       "Unable to read config file '/odm/etc/init': open() failed: No such file or directory",dr);
		return 0;
	}
	while ((de = readdir(dr)) != NULL)

		LOGD( "Parsing file %s ...", de->d_name);
	closedir(dr);
	return 0;
}

int parse_init()
{
# ifndef RECOVERY_INIT
	system_init();
	vendor_init_rc();
	product_init();
	odm_init();
# else
	recovery_init ();
# endif
}
