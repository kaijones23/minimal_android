#define LOG_TAG "init"

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

#include "init_parser.h"
#include "selinux.h"
#include "vold.h"
#include "first_stage_mount.h"
# include "log.h"

int main()
{
	prepare_log ();
	FirstStageMount();
	load_selinux();
	load_selinux_vendor();
#ifndef RECOVERY_INIT
	load_selinux_ignored();
# endif
//	load_selinux_policy ();
	load_loop();
//	vendor_init();
	parse_init();
	return 0;
}
