#define LOG_TAG "first_stage_mount"
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include "log.h"
#include "first_stage_mount.h"

//const char * system vendor odm product oem /*persist firmware */

int load_kernel_firmware()
{
	struct dirent *de;
	/*this is originally mounted by the kernel*/
	DIR *dr = opendir("proc/devicetree/firmware/android");

	while (dr == NULL) {
		LOGE("couldn't find proc/devicetree/firmware/android, skipping first_stage_mount");
		load_vendor_fstab();
		return 0;
	}
	while ((de = readdir(dr)) != NULL)
	  if (de->d_name[0] != '.') {
		LOGD(
		       "mounting /%s from /proc/devicetree/firmware/android",
		       de->d_name);
}
	closedir(dr);
	return 0;

}

int load_root_fstab()
{
	/* try to open file to read */
	FILE *file;
	const char *s = getenv("ro_boot_hardware");
	if (file = fopen("fstab."TARGET_DEVICE, "r")) {
		fclose(file);
		LOGD( "found fstab at /fstab."TARGET_DEVICE" \n", s);
	} else {
		LOGE(
		       "couldn't find /fstab."TARGET_DEVICE", rebooting to bootloader",
		       s);
		exit(-1);
	}
}

int load_vendor_fstab()
{
	/* try to open file to read */
	FILE *file;
	const char *s = getenv("ro_boot_hardware");
	if (file = fopen("vendor/etc/fstab."TARGET_DEVICE, "r")) {
		fclose(file);
		LOGD("found fstab at /vendor/etc/fstab."TARGET_DEVICE, s);
	} else {
		LOGE(
		       "couldn't find vendor/etc/fstab."TARGET_DEVICE"  passing to ramdisk fstab",s);
		load_root_fstab();
	}
}

int kernel_prepare_devs ()
{
system("dev/block/by-name/bootdevice/kernel --dev");
}

int FirstStageMount()
{
///	kernel_prepare_devs ();
	load_kernel_firmware();
}
