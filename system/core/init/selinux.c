#define LOG_TAG "selinux"
#include "selinux.h"

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "log.h"


int load_selinux()
{
	struct dirent *de;
#ifndef RECOVERY_INIT
	DIR *dr = opendir("system/etc/selinux");
#else
	DIR *dr = opendir("etc/selinux");
#endif
	if (dr == NULL) {
		LOGE("Could not open system/etc/selinux");
		return 0;
	}
	while ((de = readdir(dr)) != NULL)
	  if (de->d_name[0] != '.') {
		LOGI(
		       "loaded selinux configuration at system/etc/selinux/%s",
		       de->d_name);
		       }
	putenv("ro_selinux_enabled=true");
	closedir(dr);
	if ("permissive" == getenv("androidboot_selinux")) {
		LOGD( "your system is currently in permissive ");
	} else {
		LOGD( "your system is currently in enforcing ");
	}
	return 0;
}

int load_selinux_vendor()
{
	struct dirent *de;
	DIR *dr = opendir("vendor/etc/selinux");

	if (dr == NULL) {
		LOGE("Could not open vendor/etc/selinux");
		return 0;
	}
	while ((de = readdir(dr)) != NULL)
	  if (de->d_name[0] != '.') {
		LOGI(
		       "loaded selinux configuration at vendor/etc/selinux/%s",
		       de->d_name);
		       }
	putenv("ro_selinux_vendor_enabled=true");
	closedir(dr);
	return 0;
}

/*
int load_selinux_policy ()
{
  const char filename[] =  "system/etc/selinux/sepolicy";
   FILE *file = fopen(filename, "r");
   if ( file )
   {
	  char line [ BUFSIZ ];
	  while ( fgets(line, sizeof line, file) )
	  {
		 char substr[32], *ptr = line;
		 int n;
		 fputs(line, stdout);
		 while ( *ptr )
		 {
			if ( sscanf(ptr, "%31[^:]%n", substr, &n) == 1 )
			{
			   ptr += n;
			   puts(substr);
			}
			else
			{
			   puts("---empty field---");
			}
			++ptr;
		 }
	  }
   }
   else
   {
	  perror(filename);
   }
   return 0;
}
*/

#ifndef RECOVERY_INIT
int load_selinux_ignored()
{
	LOGD( "ignoring paths for selinux at   ");
	putenv("ro_selinux_ignored_enabled=true");
}
# endif
