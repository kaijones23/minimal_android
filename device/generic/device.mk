include $(BUILD_TOP)/vendor/magisk/vendorconfig.mk

ROOT_EXTRA_FOLDERS += \
   mnt/efi  \
   mnt/header-$(TARGET_KERNEL_HEADERS_VERSION) \
   mnt/asec \
   mnt/efi \
   mnt/uurfs \
   mnt/emmc \
   mnt/bios\
   mnt/persist \
   mnt/amonet \
   mnt/applets \

RECOVERY_EXTRA_FOLDERS += \
   mnt/efi \
   mnt/uurfs \
   mnt/emmc \
   mnt/bios\
   mnt/persist \
   mnt/amonet \
   product \
   odm \
   tmp \
   mnt/board_info \
   sbin/etc

BOOTLOADER_CMDLINE += \
androidboot_persist="mnt/persist" \
androidboot_time="0000000000"\
androidboot_kernel="/dev/bootdevice/by-name/kernel" \
androidboot_heap="4096" \
androidboot_bootloader_version="11.4" \
androidboot_secure="1" \
androidboot_reason="enabled" \
androidboot_mnt_path="/" \
androidboot_selinux_override="locked" \
androidboot_selinux_sha_crypto="256" \
androidboot_selinux_pgp_key="292929292992928384848232933209" \
androidboot_cpu_mx_nr="256" \
androidboot_usb_max_ports="24" \
androidboot_odm_name="generic" \
androidboot_override_compression="lz4" \
androidboot_kernel_init="true"\
androidboot_firstboot_wipe_data="true" \
androidboot_firstboot_wipe_data_args="fs=ext4 mount_point=/data"


RECOVERY_EXTRA_CMDLINE += \
androidboot_ro_boot_mode=recovery \
androidboot_ro_kernel_ramdisk=ro \
androidboot_ro_restore=false \
ro_recovery_image_max_sz=$(RECOVERY_IMAGE_OFFSET) \
androidboot_ro_recovery_update=true \
androidboot_ro_emmc_pagesize=4096 \
androidboot_secure_init=true \
androidboot_secure_init_key=$(shell echo $(TARGET_DEVICE) | md5sum | grep -o '^\S\+')


# skip searching the device tree because it has included all the makefiles
LOCAL_SKIP_SEARCH = "*/*"

TARGET_ADDITIONAL_SYMLINKS += \
mkdir -p $(TARGET_VENDOR_OUT)/intel/firmware && \
ln -sf intel/firmware $(TARGET_VENDOR_OUT)/firmware &&  \
ln -sf ../intel/connectivity/bt_modules $(TARGET_VENDOR_OUT)/firmware/bluetooth &&  \
ln -sf ../intel/connectivity/wlan_modules $(TARGET_VENDOR_OUT)/firmware/wlan && \
ln -sf ../intel/project/FGHWRGHN $(TARGET_VENDOR_OUT)/firmware/efi && \
ln -sf ../eng/product/generic/fstab.generic_eng   $(TARGET_VENDOR_OUT)/etc/fstab.generic_eng
