include $(BUILD_TOP)/build/target/product/generic/x86_64.mk

DEVICE_PATH=$(BUILD_TOP)/device/generic

TARGET_BOARD_PLATFORM=x86_64
TARGET_PRODUCT=intel_aosp_generic
TARGET_PRODUCT_NAME_OVERRIDE=generic
BOARD_CMDLINE_OVERRIDE += \
     ro_boot_hardware=$(TARGET_DEVICE) \
     ro_hardware=$(TARGET_DEVICE) \
     android_boot_hardware=$(TARGET_BOARD_PLATFORM) \
     $(BOOTLOADER_CMDLINE) \
     android_boot_max_shell=2 \
androidboot_persist="mnt/persist" \
androidboot_time="0000000000"\
androidboot_kernel="/dev/bootdevice/by-name/kernel" \
androidboot_heap="4096" \
androidboot_bootloader_version="11.4" \
androidboot_secure="1" \
androidboot_reason="enabled" \
androidboot_mnt_path="/" \
androidboot_selinux_override="locked" \
androidboot_selinux_sha_crypto="256" \
androidboot_selinux_pgp_key= \
androidboot_cpu_mx_nr="256" \
androidboot_usb_max_ports="24" \
androidboot_odm_name="generic" \
androidboot_override_compression="lz4" \
androidboot_kernel_init="true"\
androidboot_firstboot_wipe_data="true" \
androidboot_firstboot_wipe_data_args="fs=ext4 mount_point=/data"



TARGET_KERNEL_ARCH=x86_64
TARGET_KERNEL=kernel/generic
TARGET_DEFCONFIG=generic_defconfig
TARGET_KERNEL_HEADERS_VERSION=5.10-generic
DISPLAY_DRIVER_MAIN_DRV=$(TARGET_KERNEL)/drivers/intel/gpu
TARGET_DRIVER_FILE =$(DISPLAY_DRIVER_MAIN_DRV)/rgbaa.c

TARGET_BOOT_AGE=$(RRO_KERNEL_DATE)

TARGET_PROJECT_VERSION = 1.5-prerelease

BSP_VERSION=$(PLATFORM_VERSION)

TARGET_BOARD_PLATFORM_GENERIC=$(TARGET_BOARD_PLATFORM)-generic-$(shell date)

TARGET_HAS_BOOT_SYMLINK=true

TARGET_BOOT_SYMLINK=vmlinux

#GRUB
GRUB_BOOT_PATH += /boot
GRUB_SYMLINK += $(TARGET_BOOT_SYMLINK)
GRUB_PREBUILT_PATH =$(DEVICE_PATH)/grub.zip

POSTINSTALL_PATH = postinstall/$(TARGET_PRODUCT)

BUILD_RECOVERY_IN_ROOT := false

BOOT_LINKS += mnt/emmc/gpt

BUILD_VENDOR_IMAGE=true



BOOT_IMAGE_OFFSET += 0x0004856
SYSTEM_IMAGE_OFFSET += 0x024475
RECOVERY_IMAGE_OFFSET += 0x002547
DTB_TABLE_OFFSET += 0x0002585
MKBOOTIMG_HEADER += 0x0005423
BOOT_IMAGE_FLAGS += $(BOARD_CMDLINE_OVERRIDE) boot_image_ver=$(BSP_VERSION)

TARGET_RECOVERY_FSTAB =$(BUILD_TOP)/device/generic/recovery.fstab

TARGET_STRIP_FILES += libjiniintel_thermal.so libthermal.so librecovery_intel.so libjiniamd_thermal.so libamd_multithread.so

TARGET_BUILD_SYSTEM_AS_ROOT = true

TARGET_BOARD_MAX_CPU= 256

BOARD_BOOTIMAGE_PARTITION_SIZE := 16777216
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 20971520
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 3253731328
BOARD_CACHEIMAGE_PARTITION_SIZE := 524288000
BOARD_VENDORIMAGE_PARTITION_SIZE := 235929600
BOARD_USERDATAIMAGE_PARTITION_SIZE := 11633933824
BOARD_FLASH_BLOCK_SIZE := 2
BOOT_CMDLINE := $(BOOT_IMAGE_FLAGS)
BOOT_BASE := 0x40000000
BOOT_PAGESIZE := 4096
BOOT_OS_VERSION := 14  # Set the appropriate value for x86_64
BOOT_OS_PATCH_LEVEL := 2023-09-01  # Set the appropriate value for x86_64
KERNEL_OFFSET := 0x00000000
RAMDISK_OFFSET := 0x04000000
SECOND_OFFSET := 0x01000000
TAGS_OFFSET := 0x08000000
KERNEL_IMG := $(TARGET_PRODUCT_OUT)/kernel

# disable sepolicy in efs originally blobs from intel can only read it
TARGET_IGNORE_SEPOLICY_DIRS += /mnt/efs/etc/sepolicy/* /dev/mpt  /vendor/intel/etc/sepolicy/*

TARGET_DEVICE_PROPS=$(DEVICE_PATH)/props

TARGET_ODM_OUT=$(TARGET_VENDOR_OUT)/odm

TARGET_USES_ODM_PARTITION= true

# updater
SYSTEM_OTA_SCRIPT += $(BUILD_TOP)/build/target/product/generic/ota_start_template $(DEVICE_PATH)/extra_ota/intel_updater
CUSTOM_UPDATER_SCRIPT += $(DEVICE_PATH)/extra_ota/cust_updater $(DEVICE_PATH)/extra_ota/rpmb_updater

TARGET_BOARD_DYNAMIC_PARTITONS= "system vendor ramdisk kernel firmware"
