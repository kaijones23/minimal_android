#define LOG_TAG "watchdog"
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 500
#endif

#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

#include <time.h>
#include <ftw.h>
#include <limits.h>
#include <dirent.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>


#include "log.h"


#define WATCHDOG_DATA_PATH "dev/UQXa"
#define CONFIG_WATCHDOG_PATH "dev/UQXa"
#define MAX_LENGTH 1000025
#  define CHAR_SIZE 12


int unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
    int rv = remove(fpath);
    if (rv) perror(fpath);
    return rv;
}


int remove_dir(char *path)
{
    return nftw(path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
}


void regen_dir(char *path)
{
    int ret = 0;
    remove_dir(path);
    ret = mkdir(path, 0755);
    if (ret != 0) {
        printf("ERR: Cannot regenerate %s!\n", path);
    }
}


int getRand(int lower, int upper, int count)
{
    int i;
    for (i = 0; i < count; i++) {
        int num = (rand() %
           (upper - lower + 1)) + lower;
        return num;
    }
return 0;
}



int check_watchdog() {
  struct dirent *de;
  DIR *dr = opendir(CONFIG_WATCHDOG_PATH);

  while (dr == NULL) {
    LOGE( "waiting for watchdog daemon ");
    check_watchdog();
    return 0;
  }
  while ((de = readdir(dr)) != NULL)
  if (de->d_name[0] != '.') {
    LOGI( "bind mounting /%s to /", de->d_name);
}
  closedir(dr);
  return 0;
}

int parse_partition() {
  struct dirent *de;
#ifndef GPT
  DIR *dr = opendir("dev/block/by-name/bootdevice");
# else
  DIR *dr = opendir("dev/block/hsc.1000200/by-name/gpt/bootdevice");
# endif
  while (dr == NULL) {
    LOGE( "no partitons mounted ");
    return 0;
  }
  while ((de = readdir(dr)) != NULL)
#ifndef gpt
    LOGD( "dev/block/by-name/bootdevice/%s", de->d_name);
#else
    LOGD( "dev/block/ext4.1000200/%d/by-name/gpt/bootdevice/%s", de->d_name);
# endif
  closedir(dr);
  return 0;
}

# ifdef WATCHDOG_DEBUG
int parse_userdata() {
  struct dirent *de;
  DIR *dr = opendir(""CONFIG_WATCHDOG_PATH"/data");

  while (dr == NULL) {
    LOGE("couldn't find %s",dr);
    return 0;
  }
  while ((de = readdir(dr)) != NULL)
  if (de->d_name[0] != '.') {
  LOGI("preparing /data/%s for monitoring by watchdog_daemon", de->d_name);
  }
  closedir(dr);
  return 0;
}

int parse_system() {
  struct dirent *de;
  DIR *dr = opendir(""CONFIG_WATCHDOG_PATH"/system");

  while (dr == NULL) {
    LOGE("couldn't find %s",dr);
    return 0;
  }
  while ((de = readdir(dr)) != NULL)
  if (de->d_name[0] != '.') {
  LOGI("preparing /system/%s for monitoring by watchdog_daemon", de->d_name);
  }
  closedir(dr);
  return 0;
}

int parse_vendor() {
  struct dirent *de;
  DIR *dr = opendir(""CONFIG_WATCHDOG_PATH"/vendor");

  while (dr == NULL) {
    LOGE("couldn't find %s",dr);
    return 0;
  }
  while ((de = readdir(dr)) != NULL)
  if (de->d_name[0] != '.') {
  LOGI("preparing /vendor/%s for monitoring by watchdog_daemon", de->d_name);
  }
  closedir(dr);
  return 0;
}

# endif

int watchdog() {
/* Defines */
    srand(time(0));
    int lower = 0, upper = 100000, count = 1;
    int r = getRand(lower, upper, count);
    char cwd[PATH_MAX], rand_dir[MAX_LENGTH], full_path[MAX_LENGTH];
    FILE *WATCHDOG_DIR;
    
   /*     /* Create the random folder 
    snprintf(rand_dir, CHAR_SIZE, WATCHDOG_DATA_PATH"/%d", r);
    if (mkdir(rand_dir, 0755) != 0) {
        printf("ERR: Cannot create %s\n", rand_dir);
        return 1;
    }
*/
      /* Regenerate the temporal folder */
    regen_dir(WATCHDOG_DATA_PATH);
    
            /* Get current dir */
    if (getcwd(cwd, sizeof(cwd)) == NULL) {
        printf("ERR: Cannot determine current dir!\n");
        return 1;
    }
    
    snprintf(full_path, 100, "%s/"WATCHDOG_DATA_PATH"/%d", cwd, r);
    
  LOGD( "starting up watchdog at / ");
  LOGD( "binding root directories to watchdog directory ");
  regen_dir(CONFIG_WATCHDOG_PATH);
  system("mkdir -p "CONFIG_WATCHDOG_PATH"/mirror");
  system("ln -sf ../../../data "CONFIG_WATCHDOG_PATH"/mirror/data");
  system("ln -sf ../../../vendor "CONFIG_WATCHDOG_PATH"/mirror/vendor");
  system("ln -sf ../../../system "CONFIG_WATCHDOG_PATH"/mirror/system");
  system("ln -sf ../../../product "CONFIG_WATCHDOG_PATH"/mirror/product");
  system("ln -sf ../../../oem "CONFIG_WATCHDOG_PATH"/mirror/oem");
  system("ln -sf ../../../odm "CONFIG_WATCHDOG_PATH"/mirror/odm");
  system("ln -sf ../../../persist "CONFIG_WATCHDOG_PATH"/mirror/persist");
  putenv("ro_boot_watchdog_ready = true");
  putenv("watchdog_hidden_path=sbin/.watchdog");
  putenv("ro_boot_watchdog_hidden_path = append_from_cmdline");
  LOGD( "watchdog daemon has started ");
  LOGD( "hidden path is /");
  LOGD( "logging path is "WATCHDOG_DATA_PATH"/logging ");
  check_watchdog();
  LOGD( "disabling built in root environment ");
  system("rm -rf sbin/su system/bin/su system/xbin/su vendor/bin/su vendor/xbin/su");
  LOGI( "checking stale data on the logging directory ");
  LOGD( "printing GPT PART_TABLE ");
  parse_partition();
  LOGI( "secure memory allocated to 200MB ");
  LOGI( "current usage is 12kb ");
  LOGD( "disabled in-kernel-logging");
  system("echo 0 > sys/module/printk/watchdog_kernel_logging");
  system("echo 0 > sys/module/printk/watchdog_reboot");
  system("echo localhost:22 > sys/module/watchdog/watchdog_remote_server");
  LOGD( "running restorecon to "CONFIG_WATCHDOG_PATH"/*");
  LOGD( "restored data to data for user0 ");
  # ifdef WATCHDOG_DEBUG
  parse_system();
  parse_vendor();
  parse_userdata();
 #endif
}
