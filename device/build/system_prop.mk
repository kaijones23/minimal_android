TARGET_DEVICE_PROPS=$(BUILD_TOP)/device/$(TARGET_DEVICE)/props
SYSTEM_PROPS=$(TARGET_DEVICE_PROPS)/system.prop
VENDOR_PROPS=$(TARGET_DEVICE_PROPS)/vendor.prop
DEFAULT_PROPS=$(TARGET_DEVICE_PROPS)/default.prop
props:
	@cp -r $(VENDOR_PROPS) $(TARGET_VENDOR_OUT)/build.prop
	@cp -r $(SYSTEM_PROPS) $(TARGET_SYSTEM_OUT)/etc/build.prop
	@ln -sf etc/build.prop $(TARGET_SYSTEM_OUT)/build.prop
	@cp -r $(DEFAULT_PROPS) $(TARGET_SYSTEM_OUT)/etc/prop.default
	@ln -sf system/etc/prop.default $(TARGET_ROOT_OUT)/default.prop
.PHONY:props
