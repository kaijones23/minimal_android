# Paths and Directories
ANDROID_HOME := ~/Android/Sdk
ANDROID_NDK_HOME := $(BUILD_TOP)/prebuilts/ndk
OUT_DIR := $(BUILD_TOP)/out
TARGET_DEVICE := generic
TARGET_PRODUCT := aosp_generic
TARGET_BUILD_VARIANT := userdebug
TARGET_BUILD_TYPE := REL
TARGET_ARCH := x86_64
TARGET_ARCH_VARIANT :=
TARGET_CPU_VARIANT :=
TARGET_PRODUCT_OUT := $(OUT_DIR)/target/product/$(TARGET_DEVICE)
TARGET_SYSTEM_OUT := $(TARGET_PRODUCT_OUT)/system
TARGET_ROOT_OUT := $(TARGET_PRODUCT_OUT)/root
TARGET_VENDOR_OUT := $(TARGET_PRODUCT_OUT)/vendor
TARGET_RECOVERY_OUT := $(TARGET_PRODUCT_OUT)/recovery/root
TARGET_ODM_OUT := $(TARGET_PRODUCT_OUT)/vendor/odm
FRAMEWORK_CLASSPATH := $(shell find * -name frameworks-*-stripped.jar)
BOOT_CMDLINE := $(BOOT_IMAGE_FLAGS)

# Date and Time
date := $(shell date -u +%Y%m%d)
BUILD_DATE := $(date)

# Build Configuration
ifdef MAKE_VERBOSE
    # Verbose mode
    MAKEFLAGS += -rRnvd
else
    # Non-verbose mode
    MAKEFLAGS += -rR --no-print-directory
endif

# Include Files
include $(BUILD_TOP)/build/make/core/build_id.mk
include $(BUILD_TOP)/build/make/core/version_defaults.mk
include $(BUILD_TOP)/build/make/core/definitions.mk
include $(BUILD_TOP)/build/make/core/selinux.mk
include $(BUILD_TOP)/build/make/core/compiler.mk
include $(BUILD_TOP)/build/make/core/include.mk
include $(BUILD_TOP)/device/$(TARGET_DEVICE)/aosp_$(TARGET_DEVICE).mk
include $(BUILD_TOP)/device/$(TARGET_DEVICE)/Android.mk
-include $(BUILD_TOP)/vendor/$(TARGET_DEVICE)/*.mk

