# Compiler-related tools with LLVM and ccache
CCACHE := $(shell which ccache)

# Set ccache settings for performance optimization
export CCACHE_BASEDIR := $(PWD)
export CCACHE_NOHASHDIR := 1
export CCACHE_SLOPPINESS := time_macros,file_macro,include_file_mtime,locale
export CCACHE_COMPILERCHECK := content
export CCACHE_COMPRESSLEVEL := 5
export CCACHE_PARALLEL_COMPRESSION := true
export CCACHE_NLEVELS := 2
export CCACHE_MAXSIZE := 20G  # Set maximum cache size

# Compiler and linker settings
CC := $(CCACHE) clang
CPP := $(CCACHE) clang++
AS := $(CCACHE) llvm-as
LD := $(CCACHE) ld.lld
AR := $(CCACHE) llvm-ar
CP := $(CCACHE) cp
NM := $(CCACHE) llvm-nm
STRIP := $(CCACHE) llvm-strip
OBJCOPY := $(CCACHE) llvm-objcopy
OBJDUMP := $(CCACHE) llvm-objdump
CHECK := sparse

# Installation and system tools
INSTALLKERNEL := installkernel
DEPMOD := /sbin/depmod
PERL := perl
PYTHON := python
AWK := awk

# Copy command
COPY_COMMAND := cp

# Target-specific flags
TARGET_CPPFLAGS := $(TARGET_BOARD_CPPFLAGS) -D$(TARGET_PRODUCT) -Wall -Wextra

# Common compiler flags
CPPFLAGS := $(TARGET_CPPFLAGS)
CFLAGS := $(CPPFLAGS)
GCCFLAGS := $(CFLAGS)

# Exported variables
export CC CPP AS LD AR NM STRIP OBJCOPY OBJDUMP CHECK
export COPY_COMMAND
export TARGET_BOARD_CPPFLAGS CPPFLAGS CFLAGS GCCFLAGS
export INSTALLKERNEL DEPMOD PERL PYTHON AWK