INSTALLED_DEFAULT_PROP := $(TARGET_SYSTEM_OUT)/etc/prop.default
PGP_KEY_ID := $(shell gpg --list-keys --with-colons | grep '^pub:' | cut -d: -f5)

$(INSTALLED_DEFAULT_PROP):
	@echo "Generating $(INSTALLED_DEFAULT_PROP)"
	@echo "androidboot_persist=mnt/persist" > $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_kernel=/dev/bootdevice/by_name/kernel" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_heap=4096" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_bootloader_version=11.3" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_boot_hardware=$(TARGET_DEVICE)" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_board_name=$(TARGET_DEVICE)" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_kernel_version=$(SCM_VERSION)" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_kernel_dtb_offset=$(DTB_TABLE_OFFSET)" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_boot_build_date=$(BUILD_DATE)" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_actionable_compatible_property.enabled=false" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_postinstall_fstab_prefix=/system" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_allow_mock_location=1" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_com_google_clientidbase=android_google" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_control_privapp_permissions=enforce" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_storage_manager_enabled=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "persist_sys_dun_override=0" >> $(INSTALLED_DEFAULT_PROP)
	@echo "media_recorder_show_manufacturer_and_model=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_image_dex2oat_Xms=64m" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_image_dex2oat_Xmx=64m" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_dex2oat_Xms=64m" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_dex2oat_Xmx=512m" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_usejit=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_usejitprofiles=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_dex2oat_secondary=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_appimageformat=lz4" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_dalvik_vm_native_bridge=0" >> $(INSTALLED_DEFAULT_PROP)
	@echo "pm_dexopt_first_boot=extract" >> $(INSTALLED_DEFAULT_PROP)
	@echo "pm_dexopt_boot=extract" >> $(INSTALLED_DEFAULT_PROP)
	@echo "pm_dexopt_install=speed_profile" >> $(INSTALLED_DEFAULT_PROP)
	@echo "pm_dexopt_bg_dexopt=speed_profile" >> $(INSTALLED_DEFAULT_PROP)
	@echo "pm_dexopt_ab_ota=speed_profile" >> $(INSTALLED_DEFAULT_PROP)
	@echo "pm_dexopt_inactive=verify" >> $(INSTALLED_DEFAULT_PROP)
	@echo "pm_dexopt_shared=speed" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_dex2oat_resolve_startup_strings=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_dex2oat_max_image_block_size=524288" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_minidebuginfo=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "dalvik_vm_dex2oat_minidebuginfo=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_iorapd_enable=false" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_dynamic_partitions=$(TARGET_BOARD_DYNAMIC_PARTITIONS)" >> $(INSTALLED_DEFAULT_PROP)
	@echo "tombstoned_max_tombstone_count=50" >> $(INSTALLED_DEFAULT_PROP)
	@echo "persist_sys_usb_config=none" >> $(INSTALLED_DEFAULT_PROP)
ifneq ($(TARGET_BUILD_VARIANT),eng)
	@echo "androidboot_secure=1" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_boot_selinux=enforcing" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_sys_persist_logging=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_adb_secure=1" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_debuggable=0" >> $(INSTALLED_DEFAULT_PROP)
	@echo "debug_atrace_tags_enableflags=0" >> $(INSTALLED_DEFAULT_PROP)
else
	@echo "androidboot_secure=0" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_boot_selinux=permissive" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_sys_persist_logging=verbose" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_adb_secure=0" >> $(INSTALLED_DEFAULT_PROP)
	@echo "ro_debuggable=1" >> $(INSTALLED_DEFAULT_PROP)
	@echo "debug_atrace_tags_enableflags=1" >> $(INSTALLED_DEFAULT_PROP)
endif

ifeq ($(TARGET_BUILD_SYSTEM_AS_ROOT),true)
	@echo "androidboot_kernel_init=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_first_stage_init=true" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_reason=enabled" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_mnt_path=/" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_cpu_mx_nr=$(TARGET_BOARD_MAX_CPU)" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_usb_max_ports=24" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_odm_name=$(TARGET_DEVICE)" >> $(INSTALLED_DEFAULT_PROP)
endif

ifeq ($(TARGET_USES_FDE),true)
	@echo "androidboot_selinux_override=locked" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_selinux_sha_crypto=256" >> $(INSTALLED_DEFAULT_PROP)
	@echo "androidboot_selinux_pgp_key=$(PGP_KEY_ID)" >> $(INSTALLED_DEFAULT_PROP)
endif


.PHONY: $(INSTALLED_DEFAULT_PROP)

