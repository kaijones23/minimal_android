MAKEFILES=$(find * -name "Makefile" | sed 's/Makefile//g')
MAKE_CMD=make -C
MAKE=$(MAKE_CMD)
build_modules:
#	@$(MAKE) $(MAKEFILES)
	@$(MAKE) bionic
	@$(MAKE) bootable/recovery/amonet
	@make recovery -C bootable/recovery/
	@$(MAKE) bootable/recovery/crypto_updater
	@$(MAKE) bootable/recovery/update_engine
	@$(MAKE) system/core/init
	@$(MAKE) system/core/rootdir/
	@$(MAKE) system/core/varible/
	@#$(MAKE) system/sepolicy
	@$(MAKE) vendor/magisk
	@$(MAKE) vendor/intel
	@#$(MAKE) packages/apps/pacman4console
	@$(MAKE) vendor/intel/bootable/preloader
	@$(MAKE) vendor/intel/bootable/bootloader

