# Define common variables
OBJ := $(TARGET_PRODUCT_OUT)/obj/APEX/apex_indeterminates/$(LOCAL_APEX_MODULE)_indeterminates
OBJ2 := $(TARGET_PRODUCT_OUT)/apex/$(LOCAL_APEX_MODULE)
INCLUDES := -I$(LOCAL_C_INCLUDES)
export CFLAGS LOCAL_C_INCLUDES LOCAL_CFLAGS

# Compiler selection
ifeq ($(LOCAL_CPP_CLANG),true)
	CC = clang++
	CC_NAME = clang++
else
	CC = clang
	CC_NAME = clang
endif
export CC OBJ OBJ2 CC_NAME

# Define installation path
ifdef PROPRIETARY_MODULE
	INSTALL_EXEC_PATH := $(TARGET_VENDOR_OUT)/apex/$(LOCAL_APEX_MODULE)/bin
else
	INSTALL_EXEC_PATH := $(TARGET_SYSTEM_OUT)/apex/$(LOCAL_APEX_MODULE)/bin
endif

# Define installed APEX target
INSTALLED_APEX_TARGET := $(TARGET_SYSTEM_OUT)/apex/$(LOCAL_APEX_MODULE)

# Rule to build and install APEX
$(INSTALLED_APEX_TARGET): create_directories create_symlinks build_apex package_apex install_apex
	@echo "$(BOLD)Packed and Installed: $@$(CL_RST)"

# Rule to create necessary directories
create_directories:
	@mkdir -p $(OBJ)
	@mkdir -p $(OBJ2) $(OBJ2)/bin/
	@mkdir -p $(OBJ2)/lib $(OBJ2)/lib64

# Rule to create symbolic links to shared libraries
create_symlinks:
	@for var in $(SHARED_LIBRARIES); do \
		ln -sf ../../../$(LIBRARY_LOCATION)/$$var.so $(OBJ2)/lib/$$var.so; \
		ln -sf ../../../$(LIBRARY_LOCATION)64/$$var.so $(OBJ2)/lib64/$$var.so; \
	done

# Rule to build APEX
build_apex:
	@echo "$(BOLD)Building $(LOCAL_MODULE) <= $(LOCAL_SRC_FILES)$(CL_RST)"
	@$(CC) -g -o $(OBJ)/$(LOCAL_MODULE).o $(LOCAL_SRC_FILES)

# Rule to package APEX
package_apex:
	@cp $(OBJ)/$(LOCAL_MODULE).o $(OBJ2)/bin/$(LOCAL_MODULE)
	@echo "{ \"name\": \"$(LOCAL_APEX_MODULE)\", \"version\": \"$(VERSION)\"}" > $(OBJ2)/apex_manifest.json
	@cd $(OBJ2) && cd .. && zip --symlinks -q -r $(TARGET_SYSTEM_OUT)/apex/$(LOCAL_APEX_MODULE) $(LOCAL_APEX_MODULE)

# Rule to install APEX
install_apex:
	@cd $(BUILD_TOP)
	@echo "$(BOLD)Install: $@$(CL_RST)"

.PHONY: $(INSTALLED_APEX_TARGET) create_directories create_symlinks build_apex package_apex install_apex
