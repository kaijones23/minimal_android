OBJ=$(TARGET_PRODUCT_OUT)/obj/SHARED_LIBRARIES/$(LOCAL_MODULE)_indeterminates
RECOVERY_OBJ=$(TARGET_PRODUCT_OUT)/obj/SHARED_LIBRARIES/$(LOCAL_MODULE)_recovery_indeterminates
SRC_FILES= $(LOCAL_SRC_FILES)
INCLUDES=$(shell for inc in $(LOCAL_C_INCLUDES); do echo "-I$$inc"; done) $(shell echo $(cat out/.module_paths/EXPORT_INCLUDES ))
export CFLAGS LOCAL_C_INCLUDES
RECOVERY_CFLAGS= -DRECOVERY_LINKER -DRECOVERY_MODULE -DRECOVERY_LIBRARY -DRECOVERY_INIT -DRECOVERY_EXECUTABLE -DRECOVERY_$(TARGET_DEVICE)
ifeq ($(LOCAL_CPP_CLANG),true)
CC=echo $(BOLD)"\e[1A\e[Ktarget clang++: $(LOCAL_MODULE) <= $(LOCAL_SRC_FILES)"${CL_RST} && clang++
else
CC=echo $(BOLD)"\e[1A\e[Ktarget clang: $(LOCAL_MODULE) <= $(LOCAL_SRC_FILES)"${CL_RST} && gcc
endif
RECOVERY_LIBRARY_PATH=$(TARGET_RECOVERY_OUT)/sbin
RECOVERY_LIB_PATH=$(TARGET_RECOVERY_OUT)/sbin

ifndef INSTALL_RELATIVE_PATH
ifeq ($(PROPRIETARY_MODULE),true)
INSTALL_RELATIVE_PATH= $(TARGET_VENDOR_OUT)/lib
INSTALL_RELATIVE_PATH_64= $(TARGET_VENDOR_OUT)/lib64
else
INSTALL_RELATIVE_PATH= $(TARGET_SYSTEM_OUT)/lib
INSTALL_RELATIVE_PATH_64= $(TARGET_SYSTEM_OUT)/lib64
endif

endif

# Build hardware.TARGET_PLATFORM.so
ifeq ($(HARDWARE_MODULE),true)
ifeq ($(PROPRIETARY_MODULE),true)
INSTALL_RELATIVE_PATH= $(TARGET_VENDOR_OUT)/lib/hw
INSTALL_RELATIVE_PATH_64 = $(TARGET_VENDOR_OUT)/lib64/hw
else
INSTALL_RELATIVE_PATH= $(TARGET_ROOT_OUT)/system/lib/hw
INSTALL_RELATIVE_PATH_64 = $(TARGET_ROOT_OUT)/system/lib64/hw
endif
endif
INSTALLED_LIBRARY_TARGET=$(INSTALL_RELATIVE_PATH)/$(LOCAL_MODULE).so

$(INSTALLED_LIBRARY_TARGET):
	@mkdir -p $(OBJ)
	@mkdir -p $(INSTALL_RELATIVE_PATH) $(INSTALL_RELATIVE_PATH_64)
	@$(CC) -g  $(INCLUDES) $(CFLAGS) -o $(OBJ)/$(LOCAL_MODULE).o $(LOCAL_SRC_FILES)
	@objcopy $(OBJ)/*
ifeq ($(TARGET_ARCH),x86_64, arm64)
	@cp -r $(OBJ)/$(LOCAL_MODULE).o $(INSTALL_RELATIVE_PATH)/$(LOCAL_MODULE).so
	@cp -r $(OBJ)/$(LOCAL_MODULE).o $(INSTALL_RELATIVE_PATH_64)/$(LOCAL_MODULE).so
else
	@cp -r $(OBJ)/$(LOCAL_MODULE).o $(INSTALL_RELATIVE_PATH)/$(LOCAL_MODULE).so
endif
	@echo $(BOLD)"Install:$@"${CL_RST}
ifeq ($(INCLUDE_RECOVERY_LIBRARY),true)
	@make $(INSTALLED_RECOVERY_LIBRARY_TARGET)
endif

.PHONY:$(INSTALLED_LIBRARY_TARGET)
ifeq ($(INCLUDE_RECOVERY_LIBRARY),true)
INSTALLED_RECOVERY_LIBRARY_TARGET=$(TARGET_RECOVERY_OUT)/sbin/$(LOCAL_MODULE).so
$(INSTALLED_RECOVERY_LIBRARY_TARGET):
	@mkdir -p $(RECOVERY_OBJ)
	@$(CC) $(CFLAGS) $(INCLUDES) $(RECOVERY_CFLAGS) -o $(RECOVERY_OBJ)/$(LOCAL_MODULE).o $(LOCAL_SRC_FILES)
	@objcopy $(RECOVERY_OBJ)/*
	@cp -r $(OBJ)/$(LOCAL_MODULE).o $(RECOVERY_LIB_PATH)/$(LOCAL_MODULE).so
	@echo $(BOLD)"Install:$@"${CL_RST}
.PHONY:$(INSTALLED_RECOVERY_LIBRARY_TARGET)
endif
