include $(BUILD_TOP)/build/soong/soong_config.mk
include $(BUILD_TOP)/build/soong/soong_env.mk

# Define directory paths
BUILD_TOOLS_DIR := $(BUILD_TOP)/out/soong/tools
MODULE_PATHS_DIR := out/.module_paths
SOONG_DIR := out/soong
BUILD_TASKS_DIR := out/soong/.build_tasks

# Add $(BUILD_TOOLS_DIR) to the beginning of the PATH
PATH := $(BUILD_TOOLS_DIR):$(PATH)

# Export PATH variable
export PATH

$(SOONG_UI): 
	@echo "Starting Soong_UI $(SOONG_VERSION)-$$RANDOM"
	@prepare_directories
	@echo

prepare_directories:
	@rm -rf $(SOONG_DIR)
	@mkdir -p $(TARGET_PRODUCT_OUT)/obj/ETC/include/generated
	@mkdir -p $(MODULE_PATHS_DIR)
	@mkdir -p $(BUILD_TASKS_DIR)
	@for INC in $$(find * -name include); do \
		echo -e "\e[1A\e[KIncluding $$INC \e[0m"; \
		echo "-I$$INC" >> $(MODULE_PATHS_DIR)/EXPORT_INCLUDES; \
	done
	@find * -name Makefile -exec cat > $(SOONG_DIR)/Android-$(TARGET_DEVICE).mk "{}" +
	@for MK in $$(find * -name *.mk); do \
		echo -e "\e[1A\e[KIncluding $$MK \e[0m" ; \
	done
	@find * -name Makefile > $(MODULE_PATHS_DIR)/Makefile.list
	@find * -name Android.mk > $(MODULE_PATHS_DIR)/Android.mk.list
	@for BP in $$(find * -name *.bp); do \
		echo -e "\e[1A\e[KTarget Glob $$BP \e[0m" ; \
	done
	@find * -name Android.bp > $(MODULE_PATHS_DIR)/Android.bp.list
	@find * -name OWNERS > $(MODULE_PATHS_DIR)/OWNERS.list
	@find * -name CleanSpec.mk > $(MODULE_PATHS_DIR)/CleanSpec.mk.list
	@find * -name NOTICE > $(MODULE_PATHS_DIR)/NOTICE.list
	@find * -name *.mk > $(MODULE_PATHS_DIR)/Mk.list
	@for MAKEFILE in $$(find * -name Makefile | grep -v glibc); do \
		echo -e "\e[1A\e[KIncluding Target $$MAKEFILE \e[0m" ; \
		echo -e "\e[1A\e[K"; \
	done
	@echo -e "$(NEWLINE)Writing Build rules \\e[0m\\r"


