
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 500
#endif

#include <dirent.h>
#include <errno.h>
#include <ftw.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#ifdef RECOVERY_EXECUTABLE
#define TMP_DIR "tmp"
#define CHAR_SIZE 12
#else
#define TMP_DIR "data/local/tmp"
#define CHAR_SIZE 18
#endif

#define MAX_LENGTH 1000025
#define UPDATE_ENGINE_VER "1.2"
# define INSTALL_PATH ""

int unlink_cb(const char *fpath, const struct stat *sb, int typeflag,
              struct FTW *ftwbuf)
{
  int rv = remove(fpath);
  if (rv)
    perror(fpath);
  return rv;
}

int check_sysroot()
{
  const char *folder = "system/system";
  struct stat sb;

  if (stat(folder, &sb) == 0 && S_ISDIR(sb.st_mode))
  {
    printf("System-as-root Dectected\n");
    unlink("system_root");
    system("ln -sf system/system system_root");
    putenv("ro_recovery_system_as_root=true");
    putenv("system_build_system_as_root=true");
    return 1;
  }
  else
  {
    printf("System-as-Root is Disabled\n");
    return 0;
  }
}

int remove_dir(char *path)
{
  return nftw(path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
}

int getRand(int lower, int upper, int count)
{
  int i;
  for (i = 0; i < count; i++)
  {
    int num = (rand() % (upper - lower + 1)) + lower;
    return num;
  }
  return 0;
}

int read_result(const char *result_file)
{
  FILE *RESULT_FILE;
  int result;

  RESULT_FILE = fopen(result_file, "r");
  if (RESULT_FILE == NULL)
  {
    return -1;
  }

  fscanf(RESULT_FILE, "%d", &result);
  fclose(RESULT_FILE);

  return result;
}

char cmd[2048]; // command buffer
char *silent = ">/dev/null 2>&1"; // you'll probably use this more than once

int install_boot_patch() 
{
  printf("[amonet] install boot patch...\n");
  snprintf(cmd, 2048, "echo !BOOT | dd of=%s/dev/block/platform/by-name/boot_sig conv=notrunc %s", INSTALL_PATH, silent);
  system(cmd);
  printf("[amonet] ok..\n");
  /* Wait for the child process - Don't die here. */
  // wait(NULL); You only need to wait whenever you call fork()
}

int unmount_system()
{
  printf("Unmounting Root Partition...\n");
  /* unlink the system_root and create an empty directory */
  if (unlink("root") != -1)
  {
    mkdir("mnt/root", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    rmdir("mnt/rootfs");
    mkdir("mnt/linux_root", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    return 0;
  }
  else
  {
    printf("unlink system_root failed (errno = -1)\n");
    return -1;
  }
}

bool stat_buf(char *filename)
{
  struct stat buffer;
  return (stat(filename, &buffer) == 0);
}

void regen_dir(char *path)
{
  int ret = 0;
  remove_dir(path);
  ret = mkdir(path, 0755);
  if (ret != 0)
  {
    printf("ERR: Cannot regenerate %s!\n", path);
  }
}

int main(int argc, char *argv[])
{
  printf("Initalizing Update Engine [%s]\n", UPDATE_ENGINE_VER);

  /* Defines */
  srand(time(0));
  int lower = 0, upper = 100000, count = 1;
  int r = getRand(lower, upper, count);
  char cwd[PATH_MAX], rand_dir[MAX_LENGTH], full_path[MAX_LENGTH],
      file_contexts[MAX_LENGTH], updater_script[MAX_LENGTH],
      result_file[MAX_LENGTH];
  FILE *REC_DIR;

  /* Check input zip (argv[1]) */
  if (argc < 2)
  {
    printf("No input zip file, bailing out...\n");
    return -1;
  }

  /* Check if system as root is used */
  check_sysroot();

  /* Regenerate the temporal folder */
  regen_dir(TMP_DIR);

  /* Create the random folder */
  snprintf(rand_dir, CHAR_SIZE, TMP_DIR "/%d", r);
  if (mkdir(rand_dir, 0755) != 0)
  {
    printf("ERR: Cannot create %s\n", rand_dir);
    return 1;
  }

  /* Get current dir */
  if (getcwd(cwd, sizeof(cwd)) == NULL)
  {
    printf("ERR: Cannot determine current dir!\n");
    return 1;
  }
  snprintf(full_path, 100, "%s/" TMP_DIR "/%d", cwd, r);

  /* Set up REC_DIR */
  REC_DIR = fopen("tmp/rec_dir", "w");
  if (REC_DIR != NULL)
  {
    fputs(cwd, REC_DIR);
    fflush(REC_DIR);
    fclose(REC_DIR);
  }
  else
  {
    printf("ERR: Cannot open REC_DIR!\n");
    return 1;
  }

  /* Install the zip file */
  printf("Installing zip file '%s'...\n", argv[1]);

  /* Fork the unzip */
  if (fork() == 0)
  {
    execl("sbin/unzip",
          "sbin/unzip"
          "-qq",
          argv[1], "-d", full_path, NULL);
  }

  /* Wait for the child process - Don't die here. */
  wait(NULL);

  /* Check for file_contexts */
  snprintf(file_contexts, MAX_LENGTH, "%s/file_contexts.bin", full_path);
  if (!stat_buf(file_contexts))
  {
    printf("Zip does not contain SELinux file_contexts file in its root.\n");
  }

  /* Execute updater_script */
  snprintf(updater_script, MAX_LENGTH,
           "%s/META-INF/com/google/android/updater_script", full_path);
  printf("Checking for Digest File...\n");

  /* Unmount system */
  unmount_system();

  /* Fork the updater_script exec */
  if (fork() == 0)
  {
    execl("/bin/bash", "/bin/bash", updater_script, NULL);
  }

  /* Wait for the child process - Don't die here. */
  wait(NULL);

  /* Read the result of the installation */
  snprintf(result_file, MAX_LENGTH, "%s/result", full_path);

  /* Show the result of the installation */
  int result = read_result(result_file);
  printf("Script Suceeded. Result was [%d]\n", result);
  install_boot_patch();
  return 0;
}
