/*
 * sysroot.c
 * This file is part of sysroot
 *
 * Copyright (C) 2020 - kai
 *
 * sysroot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * sysroot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sysroot. If not, see <http://www.gnu.org/licenses/>.
 */

// this program utilizes support for system-as-root targets

#include <dirent.h>
#include <errno.h>
#include <log.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int check_sysroot() {
  const char* folder;
  folder = "system/system";
  struct stat sb;

  if (stat(folder, &sb) == 0 && S_ISDIR(sb.st_mode)) {
    printf("System-as-root Dectected");
    system("ln -sf system/system system_root");
    putenv("ro_recovery_system_as_root=true");
    putenv("system_build_system_as_root=true");
    return 1;
  } else {
    printf("System-as-Root is Disabled\n");
    return -1;
  }
}

int main() { check_sysroot(); }
