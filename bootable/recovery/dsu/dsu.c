/*
 * dsu.c
 * This file is part of DSU
 *
 * Copyright (C) 2020 - kai
 *
 * DSU is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * DSU is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSU. If not, see <http://www.gnu.org/licenses/>.
 */

/* this is dynamic updater executable that manages partitons and updates them in 
 * userland 
 */
 
/*check if we are in userspace */

# include <log.h>

# define FSTAB_DIRS "vendor/etc/fstab.%d"


int check_for_userspace ()0
{
    if(NULL == getenv("androidboot_userspace"))
    {
    printf (ALOGD "your system is currently in normal mode \n");
    }
    else
{
    printf (ALOGD "your system is currently in recovery mode, exiting \n");
    }
}

int check_fstab ()
