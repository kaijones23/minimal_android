/*
 * dex2oat.c
 * This file is part of dex2oat
 *
 * Copyright (C) 2020 - kai
 *
 * dex2oat is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * dex2oat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dex2oat. If not, see <http://www.gnu.org/licenses/>.
 */
# include <log.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

# define LOG_TAG "dex2oat"



char *readFile(char *filename) {
    FILE *f = fopen(filename, "rt");
    assert(f);
    fseek(f, 0, SEEK_END);
    long length = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *buffer = (char *) malloc(length + 1);
    buffer[length] = '\0';
    fread(buffer, 1, length, f);
    fclose(f);
    return buffer;
}


int main () {
    char *system_content = readFile("system/app/*");
    LOGD(" target dex %s", system_content);
    char *vendor_content = readFile("vendor/app/*");
    LOGD(" target dex %s", vendor_content);

}
