#include <stdio.h>
#include <log.h>
#include <unistd.h>
//#include <io.h>
#include <sys/stat.h>




# define gfx_VERSION "58.3-10.0-alps-4.0"
# define COMPILER_VERSION "clang version 10.0.0-2intel1 "
# define gfx_BRANCH "58.3-prerelease"


# define LOG_TAG "200 231  intel_gfx        "
# define MAX_FRAMEBUFFERS 2048
# define MIN_FRAMEBUFFERS 1

# define CONFIG_DVFS "enabled"


# include "display/display.c"





int isFileExists(const char *path);
int isFileExistsAccess(const char *path);
int isFileExistsStats(const char *path);



/**
 * Function to check whether a file exists or not.
 * It returns 1 if file exists at given path otherwise
 * returns 0.
 */
int isFileExists(const char *path)
{
    // Try to open file
    FILE *fptr = fopen(path, "r");

    // If file does not exists
    if (fptr == NULL)
        return 0;

    // File exists hence close file and return true.
    fclose(fptr);

    return 1;
}



/**
 * Function to check whether a file exists or not using
 * access() function. It returns 1 if file exists at
 * given path otherwise returns 0.
 */
int isFileExistsAccess(const char *path)
{
    // Check for file existence
    if (access(path, F_OK) == -1)
        return 0;

    return 1;
}



/**
 * Function to check whether a file exists or not using
 * stat() function. It returns 1 if file exists at
 * given path otherwise returns 0.
 */
int isFileExistsStats(const char *path)
{
    struct stat stats;

    stat(path, &stats);

    // Check for file existence
    if (stats.st_mode & F_OK)
        return 1;

    return 0;
}

int get_gfx_version ()
{
printf(ALOGW "gfx version  : "gfx_VERSION"\n");
printf(ALOGW "gfx branch   : "gfx_BRANCH"\n");
printf(ALOGW "gfx compiler : "COMPILER_VERSION"\n");
printf(ALOGW "intel_DVFS    : "CONFIG_DVFS" \n");
return 0;
}


int debug_buffer ()
{
int ch;
for( ch = MIN_FRAMEBUFFERS ; ch <= MAX_FRAMEBUFFERS; ch++ )
{
      printf(ALOGD"loaded framebuffer = %u, hex = %d\n", ch , ch );

}
return 0;
}


int debug_table () {
int w, h;
   get_gfx_version ();
   printf (ALOGD"loaded graphics namespace \n");
   getScreenSize(&w, &h);
   printf (ALOGD "MAIN DSPY:width = %d, height = %d \n", w, h);
   getRootWindowSize(&w, &h);
}

//this is needed for nvidia and intel controllers
int load_egl_cfg ()
{

FILE *file;
    if((file = fopen("vendor/lib/egl/egl.cfg","r"))!=NULL)
{
            // file exists
            fclose(file);
            printf(ALOGD "loaded graphics configuration at %s \n", file);

        }
    else
        {
                printf(ALOGD "no graphics configuration found! \n");
		return -2;
        }
return 0;
}

int load_kernel_graphics ()
{
  char path[100];
    if (isFileExistsAccess("/sys/class/graphics/fb0/name"))
    {
        printf(ALOGD"File exists at path '%s'\n", path);
    }
    else
    {
        printf(ALOGE"File does not exists at path '%s'\n", path);
    }
}


int main(int ac, char** av)
{

# ifdef GFX_DEBUG
load_egl_cfg ();
debug_table ();
load_kernel_graphics ();
printf(ALOGD"egl_khr process color:%i%d%d  texture:%i%d%d  fps:%u shaders:%d  vect:%d  rect:%sx%d\n");
debug_buffer ();
# else
printf(ALOGE"mapping %i%d frames to memory\n");
# endif
}
