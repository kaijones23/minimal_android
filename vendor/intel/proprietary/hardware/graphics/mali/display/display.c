#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <sys/io.h>
#include <stdlib.h>
#include <log.h>

int getRootWindowSize(int *w, int *h)
{
 Display* pdsp = NULL;
 Window wid = 0;
 XWindowAttributes xwAttr;

 pdsp = XOpenDisplay( NULL );
 printf (ALOGD"loaded display0 \n");
 if ( !pdsp ) {
  fprintf(stderr,ALOGE "Failed to open default display.\n");
  return -1;
 }

 wid = DefaultRootWindow( pdsp );
# ifdef GFX_DEBUG
printf (ALOGD"loaded screen_width \n");
printf (ALOGD"loaded screen_height \n");
# endif

 if ( 0 > wid ) {
  fprintf(stderr,ALOGE "Failed to obtain the root windows Id "
      "of the default screen of given display.\n");
  return -2;
 }

 Status ret = XGetWindowAttributes( pdsp, wid, &xwAttr );
 *w = xwAttr.width;
 *h = xwAttr.height;

 XCloseDisplay( pdsp );
 return 0;
}

int getScreenSize(int *w, int*h)
{
 int ch;
 Display* pdsp = NULL;
 Screen* pscr = NULL;

 pdsp = XOpenDisplay( NULL );
 printf (ALOGD"polled display path /dev/graphics/%d \n");
# ifdef GFX_DEBUG
 for( ch = -1024 ; ch <= -1000; ch++ )
{
      printf(ALOGW"loaded colorspace = %u \n", ch);
      printf(ALOGW"displaycontroller is actually assigned as default colorspace \n");
}
# endif
 if ( !pdsp ) {
  fprintf(stderr, "Failed to open default display.\n");
  return -1;
 }

    pscr = DefaultScreenOfDisplay( pdsp );
printf (ALOGD"found default screen \n");
 if ( !pscr ) {
  fprintf(stderr, "Failed to obtain the default screen of given display.\n");
  return -2;
 }

 *w = pscr->width;
 *h = pscr->height;

 XCloseDisplay( pdsp );
 return 0;
}
