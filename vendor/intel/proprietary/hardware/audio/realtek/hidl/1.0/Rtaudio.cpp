// playback.cpp
#include "RtAudio.h"

int main() {
    // Define constants for audio stream configuration
    const int numChannels = 2;            // Number of audio channels
    const int sampleRate = 44100;         // Sample rate in Hz
    const int bufferFrames = 256;         // Number of sample frames per buffer
    const int numBuffers = 4;             // Number of internal device buffers
    const int totalFramesToPlay = 40000;  // Total sample frames to play

    int streamId;  // The stream id
    RtAudio *audioStream;

    try {
        // Initialize the RtAudio object and open an output stream
        audioStream = new RtAudio(&streamId, 0, numChannels, 0, 0,
                                  RtAudio::RTAUDIO_FLOAT32,
                                  sampleRate, &bufferFrames, numBuffers);

        // Get a pointer to the stream buffer
        float *buffer = static_cast<float *>(audioStream->getStreamBuffer(streamId));

        // Start the audio stream
        audioStream->startStream(streamId);

        int count = 0;
        while (count < totalFramesToPlay) {
            // Generate and fill the buffer with sample frames
            // Note: Replace this part with your actual audio data generation logic

            // Trigger the output of the data buffer
            audioStream->tickStream(streamId);
            count += bufferFrames;
        }

        // Stop and close the audio stream
        audioStream->stopStream(streamId);
        audioStream->closeStream(streamId);

        // Cleanup
        delete audioStream;
    } catch (RtAudioError &e) {
        std::cerr << "Error: " << e.getMessage() << std::endl;
        return 1;
    }

    return 0;
}
