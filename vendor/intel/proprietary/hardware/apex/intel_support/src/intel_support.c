#include <stdio.h>
#include <stdlib.h>

int parse_cpu ()
{
   FILE *cpuinfo = fopen("/proc/cpuinfo", "rb");
   char *arg = 0;
   size_t size = 0;
   while(getdelim(&arg, &size, 0, cpuinfo) != -1)
   {
      puts(arg);
   }
   free(arg);
   fclose(cpuinfo);
   return 0;
}


int main ()
{
# define PLATFORM_NAMES "8086 186 286 386 486 P5 P6 NetBurst Pentium Sandy_Bridge Coffeelake"
printf ("platform support system initalized for ("PLATFORM_NAMES") \n");
parse_cpu();
}

