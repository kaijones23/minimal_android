from multiprocessing import Process

import signal
import sys
import os, time
signal.signal(signal.SIGINT, lambda x, y: sys.exit(0))

def watchdog_daemon():
     path_to_watch = "."
     before = dict ([(f, None) for f in os.listdir (path_to_watch)])
     while 1:
       time.sleep (0.1)
       after = dict ([(f, None) for f in os.listdir (path_to_watch)])
       added = [f for f in after if not f in before]
       removed = [f for f in before if not f in after]
       if added: print("Added: ", ", ".join (added))
       if removed: print("Removed: ", ", ".join (removed))
       print ("monitoring" + path_to_watch)
       before = after


if __name__ == '__main__':
    Process(target=watchdog_daemon).start()
