/// main code for preloader init.
/// copyright by Kai Jones for x86_64 targets
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <X11/Xlib.h>
#include <fb.h>
#include <generated/config.h>
#include <generated/version.h>
#include <log.h>
#include <partition.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/io.h>
#include <stdio.h>
#include <stdlib.h>

#include "display/fb0.h"
#include "utils/file.h"
#include "utils/microloader/microloader.h"
#include "utils/mounts.h"



#define SECURE_KEY "112315165115615655665654"
#define SECURE_BOOT "no"
#define TRUSTZONE "yes"
#define ARCH "x86_64"

int compiler_version() {
  printf(LOG_INFO "compiled with" COMPILER_VERSION "\n");
  return 0;
}

int rpmb_info() {
  printf(LOG_DBG "-------------RPMB info--------------\n");
  printf(LOG_DBG "SECRO_VERSION                   1.0\n");
  printf(LOG_DBG "LK_VERSION                      2.0\n");
  printf(LOG_DBG "PRELOADER_VERSION               " PRELOADER_VERSION "\n");
  printf(LOG_DBG "BOOTLOADER_VERSION 		12.0\n");
  printf(LOG_DBG "BIOS VERSION  			0   \n");
  return 0;
}

int print_partitions() {
	struct dirent *de;
	printf(LOG_DBG "availible partitions\n");
	/*this is originally mounted by the kernel*/
	DIR *dr = opendir("/build/pythonos/emmc");

	while (dr == NULL) {
		printf(LOG_ERR "couldn't find emmc device");
		return 0;
	}
	while ((de = readdir(dr)) != NULL)
	  if (de->d_name[0] != '.') {
		printf(LOG_INFO
		       "(name=%s, part_id=8 start_sect=0x%d nr_sect=0x%d \n",
		       de->d_name);
}
	closedir(dr);
	return 0;

}

int load_cmdline() {
  printf(LOG_INFO "kernel_cmdline:" CONFIG_CMDLINE "\n");
  return 0;
}

int load_partitions() {
  mount_bootloader();
  mount_recovery();
  mount_boot();
  mount_system();
  return 0;
}

int write_secure_keys ()
  {
    // This program will create same sequence of
    // random numbers on every program run

    for(int i = 0; i<10; i++)
        printf(LOG_EXP"writing %d to memory\n", rand());
    return 0;
}

int load_exploit() {
  printf(LOG_DBG "cleaned lk memory region  \n");
  printf(LOG_DBG "int secure_key returned " SECURE_KEY "\n");
  printf(LOG_EXP "clear secure key \n");
  printf(LOG_DBG "secureboot_enable " SECURE_BOOT "\n");
  write_secure_keys();
  return 0;
}

int main() {
  int w, h;
  /// load kernel early
system("./" MNT_PATH "/kernel" "");
system("./kernel" "");
#ifdef EXPLOIT
  printf(LOG_EXP "load_k64 " ARCH " kernel \n");
  printf(LOG_EXP "Starting UART at /dev/tty0 @ 12000 Baud\n");
#endif
  printf(LOG_INFO "Welcome to Intel preloader (Copyright (C) Intel 2019-2020)\n");
  compiler_version();
#ifdef EXPLOIT
  printf(LOG_DBG "Modified by kjones (Copyright (C) xda developers) \n");
#endif
  printf(LOG_INFO "[VERSION]" PRELOADER_VERSION "\n" LOG_INFO "[ARCH]" ARCH
                  "\n");
  printf(LOG_DBG "starting Display0\n");
  getScreenSize(&w, &h);
  getRootWindowSize(&w, &h);
  printf(LOG_INFO "MAIN DISPYS:width = %d, height = %d \n", w, h);
  printf(LOG_DBG "setting Partition_path to " MNT_PATH "\n");
  printf(LOG_DBG "loaded partition at " MNT_PATH "\n");
#ifdef EXPLOIT
  printf(LOG_DBG "mounted " MNT_PATH SECURE_PART "\n");
  load_microloader();
  printf(LOG_DBG
         "\nEnabled UART\nDATA_PROBE\nVERIFY_DISABLE\nUNSECURE_BOOT\n"
         "HEXDUMP_DATA\nSECUREMEM_REGION\n");
#endif
#ifndef SECURE_BOOT
  printf(LOG_INFO "Verification is disabled skipping Secure mount\n");
#endif
#ifdef EXPLOIT
  load_exploit();
  printf(LOG_EXP "Dumping old payload.img from data at 0x000152 \n");
  printf(LOG_EXP "writing 'upgrade-rpmb' " MNT_PATH BOOT_0 "\n");
  printf(LOG_ERR "rpmb:downgrade dectected!, regenerating blocks\n");
  printf(LOG_DBG "rpmb:fopen " MNT_PATH BOOT_0 " \n");
  rpmb_info();
  print_partitions();
#endif
  load_partitions();
  /*jump to bootloader instead of the kernel so the exploit can have fun */
  printf(LOG_DBG "Jumping to " ARCH " bootloader \n");
  system("./" MNT_PATH BOOTLOADER "");
#ifdef NO_ROOTFS
  system("./generic_preloader.bin");
#endif
  return 0;
}
