/*
 * mounts.c
 * This file is part of preloader
 *
 * Copyright (C) 2020 - kai
 *
 * preloader is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * preloader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with preloader. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/io.h>
#include "mounts.h"
#include "file.h"
#include <log.h>
#include <partition.h>

int mount_boot() {
  char* filename = MNT_PATH BOOT;
  int exist = cfileexists(filename);
  if (exist)
    printf(LOG_INFO "found boot at  /%s \n", filename);
  else
    printf(LOG_ERR "failed to mount boot at /%s \n", filename);
  return -1;
}

int mount_system() {
  char* filename = MNT_PATH SYSTEM;
  int exist = cfileexists(filename);
  if (exist)
    printf(LOG_INFO "found system at /%s \n", filename);
  else
    printf(LOG_ERR "failed to mount system at /%s \n", filename);
  return -1;
}

int mount_recovery() {
  char* filename = MNT_PATH RECOVERY;
  int exist = cfileexists(filename);
  if (exist)
    printf(LOG_INFO "found recovery at /%s \n", filename);
  else
    printf(LOG_ERR "failed to mount recovery at /%s \n", filename);
  return -1;
}

int mount_bootloader() {
  char* filename = MNT_PATH BOOTLOADER;
  int exist = cfileexists(filename);
  if (exist)
    printf(LOG_INFO "found bootloader at /%s \n", filename);
  else
    printf(LOG_ERR "failed to mount bootloader at /%s \n", filename);
  return -1;
}

int def_mounts() {
  printf(LOG_DBG "couldnt find /%s\n");
  printf(LOG_DBG "here are the list of available partitions\n");
  printf(LOG_DBG "mmcblocksys    " SYSTEM "\n");
  printf(LOG_DBG "mmcblockrcvry    " RECOVERY "\n");
  printf(LOG_DBG "mmcbootimg    " BOOT "\n");
  system("exit 0");
  return 0;
}
