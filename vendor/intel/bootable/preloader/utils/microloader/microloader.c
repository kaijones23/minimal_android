#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "microloader.h"
#include <partition.h>
#include <log.h>

int load_microloader() {
  printf(LOG_EXP "opened " MNT_PATH BOOT "\n");
  printf(LOG_EXP "removed microloader from " MNT_PATH BOOT "\n");
  return 0;
}
