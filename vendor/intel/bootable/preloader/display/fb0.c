#include <X11/Xlib.h>
#include <log.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/io.h>
#include "fb0.h"

int getRootWindowSize(int* w, int* h) {
  Display* pdsp = NULL;
  Window wid = 0;
  XWindowAttributes xwAttr;

  pdsp = XOpenDisplay(NULL);
  printf(LOG_DBG "loaded display0 \n");
  if (!pdsp) {
    fprintf(stderr, LOG_ERR "Failed to open default display.\n");
    return -1;
  }

  wid = DefaultRootWindow(pdsp);
  printf(LOG_DBG "loaded screen_width \n");
  printf(LOG_DBG "loaded screen_height \n");
  if (0 > wid) {
    fprintf(stderr, LOG_ERR
            "Failed to obtain the root windows Id "
            "of the default screen of given display.\n");
    return -2;
  }

  Status ret = XGetWindowAttributes(pdsp, wid, &xwAttr);
  *w = xwAttr.width;
  *h = xwAttr.height;

  XCloseDisplay(pdsp);
  return 0;
}

int getScreenSize(int* w, int* h) {
  printf(LOG_DBG "your display path is hdmi \n");
  Display* pdsp = NULL;
  Screen* pscr = NULL;

  pdsp = XOpenDisplay(NULL);
  printf(LOG_DBG "opened display path /dev/display0 \n");
  printf(LOG_DBG "avalible color options : \n");
  printf(LOG_DBG "ARGB888 ARGB555 ARGB256 BGR888 GBR558 \n");
  printf(LOG_DBG "int max res 6048x1255 \n");
  // blank fb for hiding exploit logo
  printf(LOG_EXP "blank fb \n");
  printf(LOG_DBG "waiting for lcm_status to return 1 \n");
  if (!pdsp) {
    fprintf(stderr, "Failed to open default display.\n");
    return -1;
  }

  pscr = DefaultScreenOfDisplay(pdsp);
  printf(LOG_DBG "found default screen \n");
  if (!pscr) {
    fprintf(stderr, "Failed to obtain the default screen of given display.\n");
    return -2;
  }

  *w = pscr->width;
  *h = pscr->height;

  XCloseDisplay(pdsp);
  return 0;
}
