# !/usr/bin/env python


from logger import log
import subprocess
import os
import glob
import stat
import os.path, time
import hashlib



os.environ['ro_boot_slot'] = '_a'
BOOT_SLOT=os.getenv('ro_boot_slot')


log('Getting display information...')


def get_screen_resolution():
    output = subprocess.Popen('xrandr | grep "\*" | cut -d" " -f4',shell=True, stdout=subprocess.PIPE).communicate()[0]
    resolution = output.split()[0].split(b'x')
    return {'width': resolution[0], 'height': resolution[1]}

log(get_screen_resolution())


log('Entering Fastboot')

log('Listing partitions')
for name in glob.glob('/build/pythonos/emmc/*'):
    log(name)
log('current slot is ' + BOOT_SLOT)

path = '/build/pythonos/emmc/'
size = subprocess.check_output(['du','-sh', path]).split()[0].decode('utf-8')
log("Total EMMC Usage: " + size)
