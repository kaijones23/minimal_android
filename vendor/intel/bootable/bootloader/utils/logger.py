import datetime

def log(s):
    line = "fastboot [{}] {}".format(datetime.datetime.now(), s)
    print(line)
