# ifndef C_plus_plus
# define LOG_TAG "[BOOTLOADER]"
# define LOG_DBG  "\x1B[33m[BOOTLOADER][dbg]"
# define LOG_ERR  "\x1B[31m[BOOTLOADER][err]"
# define LOG_INFO "\x1B[39m[BOOTLOADER][info]"
# define LOG_WARN "\x1B[35m[BOOTLOADER][warn]"
# else
# define LOG_TAG "[BOOTLOADER]"
# define LOG_DBG  "\x1B[33m "LOG_TAG" [dbg]"
# define LOG_ERR  "\x1B[31m "LOG_TAG" [err]"
# define LOG_INFO "\x1B[39m "LOG_TAG" [info]"
# define LOG_WARN "\x1B[35m "LOG_TAG" [warn]"
# endif
