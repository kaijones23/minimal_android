# define MNT_PATH "dev/block/by-name/bootdevice"
# define USERSPACE_MNT_PATH "../../../../dev/bootdevice/block/by-name"
# define BOOT_0 "/boot0rpmb"
# define BOOT "/boot"
# define DATA "/data"
# define LK "/lk"
# define MISC "/misc"
# define RECOVERY "/recovery"
# define SYSTEM "/system"
# define TZ "/tz"
# define SECURE_PART "/secro"
# define BOOTLOADER "/bootloader"
# define VENDOR "/vendor"
# define PRELOADER "/preloader"
# define legacy_mount "/dev/block/by-num"


typedef struct {
	char official_name[12];
	char alt_name1[12];
	char alt_name2[12];
	char alt_name3[12];
} PART_TRANS_TBL_ENTRY;
