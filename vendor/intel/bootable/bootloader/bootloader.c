/*
 * bootloader.c
 * This file is part of Bootloader
 *
 * Copyright (C) 2020 - kai
 *
 * Bootloader is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Bootloader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bootloader. If not, see <http://www.gnu.org/licenses/>.
 */

#include <generated/config.h>
#include <log.h>
#include <partition.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <linux/types.h>
#include <sys/types.h>

#define PART_META_INFO_NAMELEN  64
#define PART_META_INFO_UUIDLEN  16
#define SLOT_SUFFIX "a    "
//return normal after all the preloader is parsed
#define BOOT_MODE "\x00"

#define PART_MAX_COUNT      128


typedef enum {
	PART_OK = 0,
	PART_NOT_EXIST,
	PART_GET_DEV_FAIL,
	PART_ERASE_FAIL,
	PART_WRITE_FAIL,
	PART_READ_FAIL,
	PART_ACCESS_OVERFLOW,
} PART_OP_STATUS;


typedef struct {
	unsigned long  start_sect;
	unsigned long  nr_sects;
	unsigned int part_id;
	char *name;
	struct part_meta_info *info;
} part_t;

struct part_meta_info {
	uint8_t name[PART_META_INFO_NAMELEN];
	uint8_t uuid[PART_META_INFO_UUIDLEN];
};


part_t *partition_all;

const PART_TRANS_TBL_ENTRY g_part_name_trans_tbl[] = {
	{"misc",      "para",      "NULL",    "NULL"      },
	{"preloader_a", "PRELOADER_A", "NULL",    "NULL"      },
	{"preloader_b", "PRELOADER_B", "NULL",    "NULL"      },
	{"seccfg",    "SECCFG",    "SECCNFG", "NULL"      },
	{"uboot",     "UBOOT",     "lk",      "LK"        },
	{"boot_a",      "BOOT_A",      "bootimg", "BOOTIMG"   },
	{"boot_b",      "BOOT_B",      "bootimg", "BOOTIMG"   },
	{"recovery",  "RECOVERY",  "NULL",    "NULL"      },
	{"secro",     "SECRO",     "sec_ro",  "SEC_RO"    },
	{"logo",      "LOGO",      "NULL",    "NULL"      },
	{"system_a",    "SYSTEM_A",    "android_a", "ANDROID"},
	{"system_b",    "SYSTEM_B",    "android_b", "ANDROID"},
	{"userdata",  "USERDATA",  "usrdata", "USRDATA"   },
	{"frp",       "FRP",       "NULL",    "NULL"      },
	{"scp1",      "SCP1",      "NULL",    "NULL"      },
	{"scp2",      "SCP2",      "NULL",    "NULL"      },
	{"odmdtbo",   "ODMDTBO",   "dtbo",    "DTBO"      },
	{"NULL",      "NULL",      "NULL",    "NULL"      },
};


int partition_get_index(const char * name)
{
	int i;

	for (i = 0; i < PART_MAX_COUNT; i++) {
		if (!partition_all[i].info)
			continue;
		if (!strcmp(name, partition_all[i].info->name)) {
			printf("[%s]find %s index %d\n", __FUNCTION__, name, i);
			return i;
		}
	}

	return -1;
}

/* name: should be translated to official name first, used for backward compatible */
int partition_get_index_by_name(char *part_name)
{
	int index = -1;
	int found = 0;
	const PART_TRANS_TBL_ENTRY *entry = g_part_name_trans_tbl;

	/* if name matches one of the official names, use translation table */
	for (; 0 != strcmp(entry->official_name, "NULL"); entry++) {
		if (0 == strcmp(part_name, entry->official_name)) {
			found = 1;
			break;
		}
	}

	if (found) {
		index = partition_get_index(entry->official_name);
		if (index != -1)
			return index;

		/* try alt_name1 */
		index = partition_get_index(entry->alt_name1);
		if (index != -1)
			return index;

		/* try alt_name2 */
		index = partition_get_index(entry->alt_name2);
		if (index != -1)
			return index;

		/* try alt_name3 */
		index = partition_get_index(entry->alt_name3);
		if (index != -1)
			return index;
	} else {
		/* don't do name translation if name is not found in table */
		index = partition_get_index(part_name);
		if (index != -1)
			return index;
	}
	return index;
}

int partition_exists(char *part_name)
{
	if (partition_get_index_by_name(part_name) == -1)
		return PART_NOT_EXIST;
	else
		return PART_OK;
}

void startup() { system("rm -rf" MNT_PATH); }

int cleanup() {
  system("rm -rfv" MNT_PATH);
  return 0;
}

int g_boot_mode() {
  printf(LOG_DBG " read boot_mode from " MNT_PATH" "MISC" \n");
  return 0;
}

int append_cmdline() {
  FILE *fptr;
  // use appropriate location if you are using Linux
  fptr = fopen("proc/cmdline", "w");
  if (fptr == NULL) {
    printf(LOG_ERR "failed to open /proc/cmdline\n");
    exit(1);
  }
  fprintf(fptr, CONFIG_CMDLINE);
  fclose(fptr);
  return 0;
}

int read_cmdline() {
  system(
      "cat proc/cmdline"
      "\n");
  return 0;
}





int link_partitions() {
  system("mkdir -p " MNT_PATH);
  system("ln -sf "USERSPACE_MNT_PATH"/boot_" SLOT_SUFFIX " " MNT_PATH BOOT);
  system("ln -sf "USERSPACE_MNT_PATH"/recovery_" SLOT_SUFFIX
         "" MNT_PATH RECOVERY);
  system("ln -sf "USERSPACE_MNT_PATH"/system_" SLOT_SUFFIX "" MNT_PATH SYSTEM);
  system("ln -sf "USERSPACE_MNT_PATH"/vendor_" SLOT_SUFFIX "" MNT_PATH VENDOR);
  system("ln -sf "USERSPACE_MNT_PATH"/data    " MNT_PATH DATA);
  system("ln -sf "USERSPACE_MNT_PATH"/preloader_"SLOT_SUFFIX " " MNT_PATH PRELOADER);
  system("echo "BOOT_MODE" > "MNT_PATH"/MISC");
  return 0;
}

int link_partitions_recovery() {
  system("mkdir -p " MNT_PATH);
  system("ln -sf "USERSPACE_MNT_PATH"/boot_" SLOT_SUFFIX " " MNT_PATH BOOT);
  system("ln -sf "USERSPACE_MNT_PATH"/recovery_" SLOT_SUFFIX " " MNT_PATH RECOVERY);
  system("ln -sf "USERSPACE_MNT_PATH"/system_" SLOT_SUFFIX "" MNT_PATH SYSTEM);
  system("ln -sf "USERSPACE_MNT_PATH"/vendor_" SLOT_SUFFIX "" MNT_PATH VENDOR);
  system("ln -sf "USERSPACE_MNT_PATH"/data    " MNT_PATH DATA);
  system("ln -sf "USERSPACE_MNT_PATH"/preloader_"SLOT_SUFFIX " " MNT_PATH PRELOADER);
  system("ln -sf "USERSPACE_MNT_PATH"/bootloader_"SLOT_SUFFIX " " MNT_PATH BOOTLOADER);
  system("echo "BOOT_MODE" > "MNT_PATH"/MISC");
  return 0;
}


int load_vmlinux ()
{

FILE *file;
# ifdef GPT
    if((file = fopen("mnt/gpt/part/mmc.%d/boot/vmlinux","r"))!=NULL)
# else
    if((file = fopen("dev/block/by-name/bootdevice/kernel","r"))!=NULL)
# endif
{
            // file exists
            printf(LOG_DBG "=> Jump to k_x86_64 \n");
            fclose(file);
            printf(LOG_DBG "Found boot at "MNT_PATH BOOT"\n");
            append_cmdline();
            read_cmdline();
            link_partitions();

        }
    else
        {
                printf(LOG_ERR "No Kernel Found! \n");
		return -2;
        }
return 0;
}

int main() {
  g_boot_mode();
  printf(LOG_DBG "bootloader is loaded \n");
  load_vmlinux ();
  return 0;
}
