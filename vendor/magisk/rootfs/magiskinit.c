#include "magiskinit.h"
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#define LOG_TAG "Magisk"

// Define constants for paths
#define MAGISK_BASE "dev/mapper/.magisk"
#define MAGISK_BLOCK "dev/mapper/.magisk/block"
#define MAGISK_ROOTDIR "dev/mapper/.magisk/rootdir"
#define MAGISK_MIRROR "dev/mapper/.magisk/mirror"
/*this is an hidden container*/
#define MAGISK_DATA "opt/run/containers/install/magisk/"
#define MAGISK_VERSION "21.4"

// Function to parse partitions
int parsePartition() {
    struct dirent *de;
    DIR *dr = opendir("dev/block/by-name/bootdevice");

    if (dr == NULL) {
        printf("No partitions mounted\n");
        return 0;
    }

    while ((de = readdir(dr)) != NULL)
        printf("dev/block/by-name/bootdevice/%s\n", de->d_name);

    closedir(dr);
    return 0;
}

// Function to create mirrors
int createMirrors() {
    system("mkdir -p " MAGISK_MIRROR);
    printf("Mount: " MAGISK_MIRROR "/vendor\n");
    system("ln -sf ../../../vendor " MAGISK_MIRROR);
    printf("Mount: " MAGISK_MIRROR "/data\n");
    system("ln -sf ../../../data " MAGISK_MIRROR);
    printf("Link: " MAGISK_MIRROR "/system\n");
    system("ln -sf ../../../system " MAGISK_MIRROR);
    printf("Link: " MAGISK_MIRROR "/product\n");
    system("ln -sf ../../../product " MAGISK_MIRROR);
    return 0;
}

// Function to get block
int getBlock() {
    printf("Preparing subdirectories\n");
    parsePartition();
    system("mkdir -p " MAGISK_BLOCK);
    system("ln -sf dev/block/by-name/bootdevice/system " MAGISK_BLOCK "/system_root");
    system("ln -sf dev/block/by-name/bootdevice/vendor " MAGISK_BLOCK);
    system("ln -sf dev/block/by-name/bootdevice/cache " MAGISK_BLOCK);
    system("ln -sf dev/block/by-name/bootdevice/data " MAGISK_BLOCK);
    system("/bin/bash -c data/adb/magisk/init.modules.rc");
    system(MAGISK_ROOTDIR "/init");
    return 0;
}

// Function to prepare directories
int prepareDirs() {
    createMirrors();
    system("mkdir -p " MAGISK_BASE);
    system("mkdir -p " MAGISK_ROOTDIR);
    system("cp system/bin/init " MAGISK_ROOTDIR);
    system("mkdir -p " MAGISK_ROOTDIR "/system");
    system("cp system/etc/init/hw/init.rc " MAGISK_ROOTDIR);
    system("cp system/etc/selinux/sepolicy " MAGISK_BASE "/.se");
    getBlock();
    return 0;
}

int main() {
    printf("Magisk Init version %s\n", MAGISK_VERSION);
    prepareDirs();
    return 0;
}

